module.exports = {
  content: [],
  theme: {
    extend: {
      colors: {
        'mirage': '#161928',
        'cinder': '#0e0e18',
        'cinder-dark': '#07070c',
        'yankees-blue': '#1c2842',
      },
    },
  },
  plugins: [],
}
