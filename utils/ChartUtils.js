export const dateRangeMode = Object.freeze({
  HOUR: 'hour',
  DAY: 'day',
  WEEK: 'week',
  MONTH: 'month',
})

export const dateRangeLabel = Object.freeze({
  HOUR: '1H',
  DAY: '1D',
  WEEK: '1W',
  MONTH: '1M',
})

export const priceMode = Object.freeze({
  spot: 'spot',
  buy: 'buy',
  sell: 'sell',
})

const chartOptions = {
  chartAreaBorder: {
    borderWidth: 0,
  },
  plugins: {
    chartAreaBorder: {
      borderWidth: 0,
    },
  },
  showLine: false,
  spanGaps: true,
  animation: false,
  parsing: false,
  normalized: true,
  responsive: true,
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  datasets: {
    line: {
      pointRadius: 0,
    }
  },
  scales: {
    yAxes: [{
      ticks: {
        autoSkip: true,
        maxTicksLimit: 2,
        min: 0,
        max: 1,
        stepSize: 1,
        gridLines: {
          drawBorder: false,
        },
      },
    }],
    xAxes: [{
      display: false,
      gridLines: {
        drawBorder: false,
      },
    }],
  },
  hover: {
    mode: 'index',
    intersect: false
  },
  elements: {
    line: {
      borderColor: '#22C55E',
      borderWidth: 2,
    },
    point: {
      radius: 0,
    },
  },
  tooltips: {
    mode: 'index',
    intersect: false,
    callbacks: {},
  },
}

export const defaultChartOptions = JSON.parse(JSON.stringify(chartOptions))

export const getGradient = (ctx, chartArea) => {
  const gradient = ctx.createLinearGradient(0, chartArea.bottom, 0, chartArea.top);
  gradient.addColorStop(0, 'rgba(28, 40, 66, 0.5)');
  gradient.addColorStop(1, 'rgba(66, 184, 131, 0.5)');

  return gradient;
}

export const convertRatioToPercentFilter = (value) => {
  const prefix = value >= 0 ? '+' : ''

  return prefix + (value * 100).toFixed(2) + '%'
}
